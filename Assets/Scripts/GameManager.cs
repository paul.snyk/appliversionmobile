﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    //Gameobjects
    public GameObject cardGameObject;
    public CardController mainCardController;
    public SpriteRenderer cardSpriteRenderer;
    public RessourceManager ressourceManager;
    public GameObject moveCardBorne;
   
    //Tweaking variables
    public float fMovingSpeed;
    public float fSideMargin;
    public float fSideTrigger;
    float alphaText;
    public Color textColor;
    //UI
    public TMP_Text characterDialogue;
    public TMP_Text actionQuote;
    //Card variables
    private string leftQuote;
    private string rightQuote;
    public Card currentCard;
    public Card testCard;

    public bool dragging = false;
    private Vector2 initialPose;
    public Vector2 posCard;

    public bool borneArcade;
    private Vector2 iniPos;
    public GameObject factObject;
    void Start()
    {
        LoadCard(testCard);
        

        iniPos = cardGameObject.transform.position;
    }
    void UpdateDialogue()
    {
        actionQuote.color = textColor;
        if (cardGameObject.transform.position.x > 0)
        {
            actionQuote.text = leftQuote;
        }
        else
        {
            actionQuote.text = rightQuote;
        }
    }
    void Update()
    {
        //Dialogue text
        textColor.a = Mathf.Min((Mathf.Abs(cardGameObject.transform.position.x - fSideMargin) / 0.5f), 1);
        if (cardGameObject.transform.position.x > fSideTrigger)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
            UpdateDialogue();
            if (Input.GetMouseButtonUp(0)|| Input.GetButtonUp("Jump"))
            {
                currentCard.Right();
                cardGameObject.transform.position = new Vector3(0, cardGameObject.transform.position.y);
                NewCard();
                Debug.Log("Right");
            }
        }
        //Le fondu du texte
        else if (cardGameObject.transform.position.x > fSideMargin)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
        }
        else if (cardGameObject.transform.position.x > -fSideMargin)
        { 
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
        }
        else if (cardGameObject.transform.position.x > -fSideTrigger)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
            UpdateDialogue();
        }
        else
        {
            if (Input.GetMouseButtonUp(0)|| Input.GetButtonUp("Jump"))
            {
                currentCard.Left();
                cardGameObject.transform.position = new Vector3(0, cardGameObject.transform.position.y);
                NewCard();
            }
        }

        UpdateDialogue();
        //Mouvement
        if (!borneArcade)
        {
            if (Input.GetMouseButtonDown(0)|| Input.GetButtonDown("Jump"))
            {
                dragging = true;
                initialPose = Camera.main.ScreenToWorldPoint(new Vector2( Input.mousePosition.x, 0));
            }
            else if (Input.GetMouseButtonUp(0)|| Input.GetButtonUp("Jump"))
            {
                dragging = false;
                cardGameObject.transform.position = iniPos;
            }
        }
        else
        {
            cardGameObject.transform.position = new Vector3(moveCardBorne.transform.position.x, cardGameObject.transform.position.y);
            if (factObject.activeSelf && Input.GetButtonDown("Jump"))
            {
                cardGameObject.transform.position = new Vector3(0,cardGameObject.transform.position.y);
                moveCardBorne.transform.position = new Vector3(0,moveCardBorne.transform.position.y);
                GameObject.FindObjectOfType<moveCardJoystick>().positionjoystick = true;
                Debug.Log("sbkdgjvhsndv");
                GameObject.FindObjectOfType<FactDisplay>().ReturnCard();
            }
        }


        if (dragging)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, 0));
            Vector2 move = pos - initialPose;
            cardGameObject.transform.position = posCard + move;
        }

        LoadCard(testCard);
    }
    void OnMouseUp()
    {
        //La carte va à gauche ou à droite
        if (!Input.GetMouseButton(0) && cardGameObject.transform.position.x > fSideTrigger)
        {
            currentCard.Right();
        }
        else if (!Input.GetMouseButton(0) && cardGameObject.transform.position.x > fSideTrigger)
        {
            currentCard.Left();
        }
    }

    public void LoadCard(Card card)
    {
        //Charge les infos de la carte
        leftQuote = card.leftQuote;
        rightQuote = card.rightQuote;
        currentCard = card;
        characterDialogue.text = card.dialogue;
    }

    public void NewCard()
    {
        GameObject.FindObjectOfType<DataBase>().NextText();
        
    }
}
